
/////////////
///VARIABLES
/////////////
var cards = ['2H', '3H', '4H', '5H', '6H', '7H', '8H', '9H', 'TH', 'JH', 'QH', 'KH', 'AH'
            ,'2S', '3S', '4S', '5S', '6S', '7S', '8S', '9S', 'TS', 'JS', 'QS', 'KS', 'AS'
            ,'2D', '3D', '4D', '5D', '6D', '7D', '8D', '9D', 'TD', 'JD', 'QD', 'KD', 'AD'
            ,'2C', '3C', '4C', '5C', '6C', '7C', '8C', '9C', 'TC', 'JC', 'QC', 'KC', 'AC' ];
var table = [];

var hand = [];
var com1 = []
   ,com2 = []
   ,com3 = [];
var selectedCard = 0 ;
var comSelection = 0;

var playerBid = 0;
var playerScore = 0;




function shuffle(o){
     
     for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), 
        x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};



/////////////
///DEAL CARDS
/////////////
function dealCards(){
    hand = [];
    com1 = [];
    com2 = [];
    com3 = [];
    cards = shuffle(cards);

    document.getElementById("1").disabled = true;
    document.getElementById("2").disabled = true;
    document.getElementById("3").disabled = true;
    document.getElementById("4").disabled = true;//jquerry
    document.getElementById("5").disabled = true;
    document.getElementById("6").disabled = true;
    document.getElementById("7").disabled = true;
    document.getElementById("8").disabled = true;
    document.getElementById("9").disabled = true;
    document.getElementById("10").disabled = true;
    document.getElementById("11").disabled = true;
    document.getElementById("12").disabled = true;
    document.getElementById("13").disabled = true;
    document.getElementById("allCards").innerHTML =("All Deck: " + cards);
    for(var i=0; i < 13;i++){
        hand.push(cards[i]);
        com1.push(cards[i+13]);
        com2.push(cards[i+26]);
        com3.push(cards[i+39]);
    };
    
    document.getElementById("playerHand").innerHTML = "Player Hand: " + hand;
    document.getElementById("com1Hand").innerHTML = "Com1 Hand: " + com1;
    document.getElementById("com2Hand").innerHTML = "Com2 Hand: " + com2;
    document.getElementById("com3Hand").innerHTML = "Com3 Hand: " + com3;
    alert("Select A Bid");
};

//////////
///BUTTONS
//////////
function reply_click(clicked_id)
{
    document.getElementById("output").innerHTML = "";
    table = [];
    

    switch(clicked_id)
    {
        case '1':
        selectedCard = 0;
        document.getElementById("1").disabled = true;
        gameFlow();
         break;
        case '2':
        selectedCard = 1;
        document.getElementById("2").disabled = true;
        gameFlow();
         break;
        case '3':
        selectedCard = 2;
        document.getElementById("3").disabled = true;
        gameFlow();
         break;
        case '4':
        selectedCard = 3;
        document.getElementById("4").disabled = true;
        gameFlow();
         break;
        case '5':
        selectedCard = 4;
        document.getElementById("5").disabled = true;
        gameFlow();
         break;
        case '6':
        selectedCard = 5;
        document.getElementById("6").disabled = true;
        gameFlow();
         break;
        case '7':
        selectedCard = 6;
        document.getElementById("7").disabled = true;
        gameFlow();
         break;
        case '8':
        selectedCard = 7;
        document.getElementById("8").disabled = true;
        gameFlow();
         break;
        case '9':
        selectedCard = 8;
        document.getElementById("9").disabled = true;
        gameFlow();
         break;
        case '10':
        selectedCard = 9;
        document.getElementById("10").disabled = true;
        gameFlow();
         break;
        case '11':
        selectedCard = 10;
        document.getElementById("11").disabled = true;
        gameFlow();
         break;
        case '12':
        selectedCard = 11;
        document.getElementById("12").disabled = true;
        gameFlow();
         break;
        case '13':
        selectedCard = 12;
        document.getElementById("13").disabled = true;
        gameFlow();
         break;
        case 'deal':
        dealCards();
         break;
        case 'bid0':
        playerBid = 0;
        disableBidButtons();
         break;
         case 'bid1':
        playerBid = 1;
        disableBidButtons();
         break;
         case 'bid2':
        playerBid = 2;
        disableBidButtons();
         break;
         case 'bid3':
        playerBid = 3;
        disableBidButtons();
         break;
         case 'bid4':
        playerBid = 4;
        disableBidButtons();
         break;
         case 'bid5':
        playerBid = 5;
        disableBidButtons();
         break;
         case 'bid6':
        playerBid = 6;
        disableBidButtons();
         break;
         case 'bid7':
        playerBid = 7;
        disableBidButtons();
         break;
         case 'bid8':
        playerBid = 8;
        disableBidButtons();
         break;
         default:
         alert("something wrong");
    
    };
};
///////////
///GAMEFLOW
///////////
function disableBidButtons(){
    document.getElementById("bid0").disabled = true;
    document.getElementById("bid1").disabled = true;
    document.getElementById("bid2").disabled = true;
    document.getElementById("bid3").disabled = true;
    document.getElementById("bid4").disabled = true;
    document.getElementById("bid5").disabled = true;
    document.getElementById("bid6").disabled = true;
    document.getElementById("bid7").disabled = true;
    document.getElementById("bid8").disabled = true;
    document.getElementById("output").innerHTML += 'Bid: ' + playerBid;
    document.getElementById("1").disabled = false;
    document.getElementById("2").disabled = false;
    document.getElementById("3").disabled = false;
    document.getElementById("4").disabled = false;
    document.getElementById("5").disabled = false;
    document.getElementById("6").disabled = false;
    document.getElementById("7").disabled = false;
    document.getElementById("8").disabled = false;
    document.getElementById("9").disabled = false;
    document.getElementById("10").disabled = false;
    document.getElementById("11").disabled = false;
    document.getElementById("12").disabled = false;
    document.getElementById("13").disabled = false;
};

function gameFlow(){
    table.push(hand[selectedCard]);

    for(var x=0;x<4;x++){
        comSelection = Math.floor((Math.random()*12)+0);
        if(x == 1){
            table.push(com1[comSelection]);
        };
        if(x == 2){
            table.push(com2[comSelection]);
        };
        if(x == 3){
            table.push(com3[comSelection]);
        };

    };
    document.getElementById("output").innerHTML = "Table: " + table + '<br>';
    convertTens();
    
    document.getElementById("output").innerHTML += parseInt(table[0]) + '<br>';
    document.getElementById("output").innerHTML += parseInt(table[1]) + '<br>';
    document.getElementById("output").innerHTML += parseInt(table[2]) + '<br>';
    document.getElementById("output").innerHTML += parseInt(table[3]) + '<br>';
    compareTable();
   
};

function convertTens(){

    
    switch(table[0][0])
    {
        case 'T':
         table.splice(0, 1, "10" + table[1][1]);
         break;
        case 'J':
         table.splice(0, 1, "11" + table[1][1]);
         break;
        case 'Q':
         table.splice(0, 1, "12" + table[1][1]);
         break;
        case 'K':
         table.splice(0, 1, "13" + table[1][1]);
         break;
        case 'A':
         table.splice(0, 1, "14" + table[1][1]);
         break;
    };

    switch(table[1][0])
    {
        case 'T':
         table.splice(1, 1, "10" + table[1][1]);
         break;
        case 'J':
         table.splice(1, 1, "11" + table[1][1]);
         break;
        case 'Q':
         table.splice(1, 1, "12" + table[1][1]);
         break;
        case 'K':
         table.splice(1, 1, "13" + table[1][1]);
         break;
        case 'A':
         table.splice(1, 1, "14" + table[1][1]);
         break;
    };
    switch(table[2][0])
    {
        case 'T':
         table.splice(2, 1, "10" + table[2][1]);
         break;
        case 'J':
         table.splice(2, 1, "11" + table[2][1]);
         break;
        case 'Q':
         table.splice(2, 1, "12" + table[2][1]);
         break;
        case 'K':
         table.splice(2, 1, "13" + table[2][1]);
         break;
        case 'A':
         table.splice(2, 1, "14" + table[2][1]);
         break;
    };
    switch(table[3][0])
    {
        case 'T':
         table.splice(3, 1, "10" + table[3][1]);
         break;
        case 'J':
         table.splice(3, 1, "11" + table[3][1]);
         break;
        case 'Q':
         table.splice(3, 1, "12" + table[3][1]);
         break;
        case 'K':
         table.splice(3, 1, "13" + table[3][1]);
         break;
        case 'A':
         table.splice(3, 1, "14" + table[3][1]);
         break;
    };
};


function compareTable(){

    if(parseInt(table[0]) > (parseInt(table[1]) && parseInt(table[2]) && parseInt(table[3]))){
        document.getElementById("output").innerHTML += "Player Win";
    }
    else if(parseInt(table[1]) > (parseInt(table[0]) && parseInt(table[2]) && parseInt(table[3]))){
        document.getElementById("output").innerHTML += "Computer 1 Win";
    }
    else if(parseInt(table[2]) > (parseInt(table[1]) && parseInt(table[0]) && parseInt(table[3]))){
        document.getElementById("output").innerHTML += "Computer 2 Win";
    }
    else if(parseInt(table[3]) > (parseInt(table[1]) && parseInt(table[2]) && parseInt(table[0]))){
        document.getElementById("output").innerHTML += "Computer 3 Win";
    };
  
};



//////////
//inits
/////////

dealCards();

////////////



/////////////////
///REMOVE HEREEE
/////////////////
/*
array.splice(index, 1);
Math.max(5,10);

*/
/////////////////
///REMOVE HEREEE
/////////////////
