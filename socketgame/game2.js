
var util = require("util"),                 
io = require("socket.io"),              
Player = require("./Player").Player;  

var socket,     // Socket controller
    players;    // player array


function init(){
    socket = io.listen(8000);
    socket.configure(function() {
        socket.set("transports", ["websocket"]);
        socket.set("log level", 2);
    });
 };

 init();